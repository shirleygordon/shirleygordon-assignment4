#include "File.h"

/*
File C'tor, opens file path (name) with specified mode.
Input: file path, mode.
Output: none.
*/
File::File(const string& name, const char* mode)
{
	const char* file_path = name.c_str(); // Convert string to char*.
	
	this->_file_fd = fopen(file_path, mode); // Open file.
	
	if (this->_file_fd == NULL) // Exit program if file failed to open.
	{
		cerr << "Error opening file." << endl;
		_exit(1);
	}
}

/*
Implicit conversion function to FILE*.
Input: none.
Output: none.
*/
File::operator FILE* ()
{
	return this->_file_fd;
}

/*
D'tor for File object, closes open file.
Input: none.
Output: none.
*/
File::~File()
{
	if (this->_file_fd != NULL)
	{
		fclose(this->_file_fd);
	}
}
