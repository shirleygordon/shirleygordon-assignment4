#pragma once
#include "File.h"
#include <vector>
using std::vector;
using std::cout;

#define NOT_FOUND -1
#define MAX_SIZE 1024

int scanFile(const char* substr, const string& file_path, vector<string>& lines);
void printLines(const int instances, vector<string>& lines, const char* substr, const char* file_path);