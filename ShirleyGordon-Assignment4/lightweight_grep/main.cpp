#include "File.h"
#include "lightweight_grep.h"
using std::cout;

#define MIN_ONE_FILE 3
#define FIRST_FILE_INDEX 2 // The first file path is at index 2 of argv.
#define SUBSTR_INDEX 1

int main(int argc, char** argv)
{
	int instances = 0;
	unsigned int i = 0, j = 0;
	vector<string> lines;

	// Print welcome message
	cout << "Welcome to GrepLight!" << endl << endl;

	// If there isn't at least one file provided by the user, exit the program.
	if (argc < MIN_ONE_FILE) 
	{
		cerr << "Please provide at least one file path." << endl;
		_exit(1);
	}

	// Start scanning files for substring.
	for (i = FIRST_FILE_INDEX; i < argc; i++)
	{
		cout << argv[i] << endl; // Print file name.

		instances = scanFile(argv[SUBSTR_INDEX], argv[i], lines); // Scan file for occurrences of substring.
		printLines(instances, lines, argv[SUBSTR_INDEX], argv[i]); // Print the lines of file the substring was found in.
	}
}