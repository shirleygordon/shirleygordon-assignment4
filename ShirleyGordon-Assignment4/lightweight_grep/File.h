#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <cstdio>
#include <string>
#include <iostream>
using std::cerr;
using std::endl;
using std::string;

// The class responsible for holding FILE pointer - RAII implementation of FILE resource.
class File
{
private:
	FILE* _file_fd;

public:
	File(const string& name, const char* mode); // File C'tor, open file path (name) with specified mode. 
	operator FILE* (); // Implicit conversion function to FILE*.
	~File(); // D'tor for File object, closes open file.
};