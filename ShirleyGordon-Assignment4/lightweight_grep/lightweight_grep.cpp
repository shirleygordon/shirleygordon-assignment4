#include "lightweight_grep.h"

/*
Function scans the given file for instances of the given substring.
Input: substring, file path and vector of strings to save the lines the substring was found in.
Output: number of instances of substring found, or -1 if substring wasn't found in file.
*/
int scanFile(const char* substr, const string& file_path, vector<string>& lines)
{
	File file(file_path, "r");
	int instances = 0;
	char* currLine = new char[MAX_SIZE];

	// Scan entire file for occurrences of substring.
	while (fgets(currLine, MAX_SIZE, file) != NULL)
	{
		// If substring found in the current line:
		if (strstr(currLine, substr) != NULL)
		{
			instances++; // Increase instances.
			lines.push_back(currLine); // Push back the current line into lines vector.
		}
	}

	// If no occurrences of substring found in entire file:
	if (instances == 0)
	{
		instances = NOT_FOUND; // Set instances to -1.
	}

	delete[] currLine;
	return instances;
}

/*
Function prints the lines of the file the substring was found in.
Input: number of occurrences of substring in file, vector of strings containing the lines.
Output: none.
*/
void printLines(const int instances, vector<string>& lines, const char* substr, const char* file_path)
{
	unsigned int i = 0;

	// If substring wasn't found in file:
	if (instances == NOT_FOUND)
	{
		cout << "No instances of pattern " << substr << " found in " << file_path << endl;
	}
	else // If substring was found in file:
	{
		cout << "Found " << instances << " instances of pattern " << substr << " in file " << file_path << endl;

		// Print each line the substring was found in.
		for (i = 0; i < lines.size(); i++)
		{
			cout << "	-> " << lines[i];
		}

		lines.resize(0); // Empty the lines vector, in order to prepare it for the next scan.
	}
}
