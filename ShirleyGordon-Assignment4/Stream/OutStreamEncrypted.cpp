#include "OutStreamEncrypted.h"

/*
Function prints the encrypted string.
Input: string.
Output: OutStreamEncrypted reference.
*/
OutStreamEncrypted& OutStreamEncrypted::operator<<(const char* str)
{
	unsigned int i = 0;
	char* encrypted = new char[strlen(str) + 1]; // Create a new char array the size of str

	strcpy(encrypted, str); // Copy str into the new char array

	// Encrypt the string
	for(i = 0; i < strlen(str); i++)
	{
		if (encrypted[i] >= MIN_ASCII && encrypted[i] <= MAX_ASCII) // Encrypt only if the character is between 32 and 126
		{
			if (encrypted[i] + offset > MAX_ASCII) // If the current character + offset is bigger than 126
			{
				encrypted[i] = (encrypted[i] + offset) % MAX_ASCII + MIN_ASCII; // Change the character circularly
			}
			else
			{
				encrypted[i] = encrypted[i] + offset; // Add the offset to the current character
			}
		}
	}
	
	fprintf(this->_file, "%s", encrypted);

	delete[] encrypted;
	return *this;
}

/*
Function prints the encrypted number.
Input: integer num.
Output: OutStreamEncrypted reference.
*/
OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char* str = new char[to_string(num).length() + 1]; // Create a new char array with length of num

	_itoa(num, str, DECIMAL_BASE); // Convert int to char array
	this->operator<<(str); // Pass char array to operator<< to encrypt and print it

	delete[] str;
	return *this;
}

/*
Function calls the passed function pointer.
Input: function pointer.
Output: OutStreamEncrypted reference.
*/
OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)(FILE* file))
{
	pf(this->_file);
	return *this;
}
