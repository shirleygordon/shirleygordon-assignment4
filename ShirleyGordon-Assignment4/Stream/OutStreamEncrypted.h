#pragma once
#include "OutStream.h"

#define MIN_ASCII 32
#define MAX_ASCII 126
#define DECIMAL_BASE 10

using std::to_string;

class OutStreamEncrypted : protected OutStream
{
private:
	int offset;

public:
	OutStreamEncrypted(int offset) : OutStream() { this->offset = offset; };
	~OutStreamEncrypted() {};

	OutStreamEncrypted& operator<<(const char* str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)(FILE* file));
};