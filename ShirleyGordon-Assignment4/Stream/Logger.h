#pragma once
#include "OutStream.h"
#include "FileStream.h"

class Logger
{
private:
	OutStream _logStream;
	FileStream _logFile;
	bool _logToScreen;
	static unsigned int _logNum;

public:
	Logger(const char* filename, bool logToScreen) : _logFile(filename) { this->_logToScreen = logToScreen; };
	~Logger() {};

	void print(const char* msg);
};
