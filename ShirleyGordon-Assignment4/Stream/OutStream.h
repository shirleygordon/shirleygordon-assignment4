#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string>

class OutStream
{
protected:
	FILE* _file;

public:
	OutStream();
	OutStream(const char* filePath);
	~OutStream();

	OutStream& operator<<(const char *str);
	OutStream& operator<<(int num);
	OutStream& operator<<(void(*pf)(FILE* file));
};

void endline(FILE* file);