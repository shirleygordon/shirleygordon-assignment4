#include "Logger.h"

unsigned int Logger::_logNum = 1;

/*
Function logs events that occur while the program is running
to log file and to screen if logToScreen is true.
Input: message to log.
Output: none.
*/
void Logger::print(const char* msg)
{
	this->_logFile << _logNum << ". " << msg << endline; // Log message to log file

	if (this->_logToScreen) // If logToScreen is true
	{
		this->_logStream << _logNum << ". " << msg << endline; // Log message to screen
	}

	_logNum++;
}
