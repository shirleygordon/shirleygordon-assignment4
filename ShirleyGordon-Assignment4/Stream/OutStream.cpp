#include "OutStream.h"

/*
Default constructor initializes file pointer of
OutStream class as stdout.
Input: none.
Output: none.
*/
OutStream::OutStream()
{
	this->_file = stdout; // Default output file is stdout.
}

/*
Constructor of OutStream initializes file pointer of
OutStream class as the specified file.
Input: none.
Output: none.
*/
OutStream::OutStream(const char* filePath)
{
	this->_file = fopen(filePath, "a");
}

/*
Destructor of OutStream object.
Input: none.
Output: none.
*/
OutStream::~OutStream()
{
	if (this->_file != NULL)
	{
		fclose(this->_file);
	}
}

/*
Function prints a string (char array).
Input: char array pointer.
Output: reference to OutStream object.
*/
OutStream& OutStream::operator<<(const char *str)
{
	fprintf(this->_file, "%s", str);
	return *this;
}

/*
Function prints an integer.
Input: integer.
Output: reference to OutStream object.
*/
OutStream& OutStream::operator<<(int num)
{
	fprintf(this->_file, "%d", num);
	return *this;
}

/*
Function calls the function passed to it.
Input: function pointer which receives file pointer as an argument.
Output: reference to OutStream object.
*/
OutStream& OutStream::operator<<(void(*pf)(FILE* file))
{
	pf(this->_file);
	return *this;
}

/*
Function prints a newline.
Input: none.
Output: none.
*/
void endline(FILE* file)
{
	fprintf(file, "\n");
}
