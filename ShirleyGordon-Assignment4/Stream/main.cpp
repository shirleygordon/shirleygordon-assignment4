#include "OutStream.h"
#include "FileStream.h"
#include "OutStreamEncrypted.h"
#include "Logger.h"

int main(int argc, char **argv)
{
	OutStreamEncrypted encryptedStream(4);
	FileStream file("newFile.txt");
	OutStream stream;
	Logger logger("log.txt", true);
	Logger logger2("log2.txt", true);
	
	// Test printing to screen
	logger.print("Printing to screen...");
	stream << "I am the Doctor and I am " << 1500 << " years old" << endline;
	logger2.print("Printing to screen completed.");

	// Test writing to file
	logger.print("Writing to file...");
	file << "I am the Doctor and I am " << 1500 << " years old" << endline;
	logger2.print("Writing to file completed.");
	
	// Test OutStreamEncrypted
	logger.print("Encrypting...");
	encryptedStream << "I am the Doctor and I am " << 1500 << " years old" << endline;
	logger2.print("Printing encrypted string completed.");
	
	return 0;
}
